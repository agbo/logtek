if [ -f "output.json" ]; then
	sed '$i`sh allhosts.sh | tr -d "\n"`' output.json
else
	echo "{" >> output.json
	sh allhosts.sh | tr -d "\n" >> output.json
	echo "}" >> output.json
fi 
