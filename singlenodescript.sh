# Get VM Hostname
i=$1
hostname=`ssh $i hostname` 2> /dev/null

# Get Linux Distribution
distro=`ssh $i 'python -c "import platform ; print platform.linux_distribution()[0]"'` 2>/dev/null
version=`ssh $i 'python -c "import platform; print platform.linux_distribution()[1]"'` 2> /dev/null

# Get Server uptime
if [ -f "/proc/uptime" ]; then
	uptime=`ssh $i cat /proc/uptime`
	uptime=${uptime%%.*}
	seconds=$(( uptime%60 ))
	minutes=$(( uptime/60%60 ))
	hours=$(( uptime/60/60%24 ))
	days=$(( uptime/60/60/24 ))
	uptime="$days days, $hours hours, $minutes minutes, $seconds seconds"
else
	uptime=""
fi

# Get VM private IP Address
IPAddress=`ssh $i ip addr | grep 'state UP' -A2 | tail -n1 | awk '{print $2}' | cut -f1  -d'/'` 2> /dev/null

# Get VM Public IP Address
PublicIP=`ssh $i wget http://ipecho.net/plain -O - -q ; echo` 2> /dev/null

# Get the number of CPUs
NumberOfCPUs=`ssh $i grep -c ^processor /proc/cpuinfo` 2> /dev/null

# Get the Average CPU Load
CPULoad=`ssh $i top -bn1 | grep load | awk '{printf "%.2f\n", $(NF-2)}'` 2> /dev/null

# Get the total Memory
TotalMemoryMB=`ssh $i grep MemTotal /proc/meminfo | awk '{print $2}'` 2> /dev/null

# Get percentage of memory in use
MemoryInUse=`ssh $i free | grep Mem | awk '{print $3/$2 * 100.0}'` 2> /dev/null

# Get percentage of free memory
FreeMemory=`ssh $i free | grep Mem | awk '{print $4/$2 * 100.0}'` 2> /dev/null

printf '{"hostname":"%s","distro":"%s","version":"%s","uptime":"%s","IPAddress":"%s","PublicIP":"%s","NumberOfCPUs":"%s","CPULoad":"%s","TotalMemoryMB":"%s","MemoryInUse":"%s","FreeMemory":"%s"}\n' "$hostname" "$distro" "$version" "$uptime" "$IPAddress" "$PublicIP" "$NumberOfCPUs" "$CPULoad" "$TotalMemoryMB" "$MemoryInUse" "$FreeMemory"
