printf "{\"`date +%s`\":{ \"Hosts\":["
LENGTH=`cat hosts | grep -o '^[^#]*' |sed '/^\s*$/d' | wc -l`
ITERATOR=1
for i in `cat hosts | grep -o '^[^#]*' |sed '/^\s*$/d' `; do 
	ssh $i "`cat script.sh`" 2> /dev/null
	if [ "$ITERATOR" -lt "$LENGTH" ]; then
		echo ","
	fi
ITERATOR=$(expr $ITERATOR + 1 )
done
printf "]}}"
