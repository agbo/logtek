# Get VM Hostname
hostname=`hostname` 2> /dev/null

# Get Linux Distribution
distro=`python -c 'import platform ; print platform.linux_distribution()[0] + " " +        platform.linux_distribution()[1]'` 2> /dev/null

# Get Server uptime
if [ -f "/proc/uptime" ]; then
	uptime=`cat /proc/uptime`
	uptime=${uptime%%.*}
	seconds=$(( uptime%60 ))
	minutes=$(( uptime/60%60 ))
	hours=$(( uptime/60/60%24 ))
	days=$(( uptime/60/60/24 ))
	uptime="$days days, $hours hours, $minutes minutes, $seconds seconds"
else
	uptime=""
fi

# Get VM private IP Address
IPAddress=`ip addr | grep 'state UP' -A2 | tail -n1 | awk '{print $2}' | cut -f1  -d'/'` 2> /dev/null

# Get VM Public IP Address
PublicIP=`wget http://ipecho.net/plain -O - -q ; echo` 2> /dev/null

# Get the number of CPUs
NumberOfCPUs=`grep -c ^processor /proc/cpuinfo` 2> /dev/null

# Get the Average CPU Load
CPULoad=`top -bn1 | grep load | awk '{printf "%.2f\n", $(NF-2)}'` 2> /dev/null

# Get the total Memory
TotalMemoryMB=`grep MemTotal /proc/meminfo | awk '{print $2}'` 2> /dev/null

# Get percentage of memory in use
MemoryInUse=`free | grep Mem | awk '{print $3}'` 2> /dev/null

# Get percentage of free memory
FreeMemory=`free | grep Mem | awk '{print $4}'` 2> /dev/null

Disks=`df -Ph | awk '/^\// {print $1"\t"$2"\t"$4}' | python -c 'import json, fileinput; print json.dumps({"DiskArray":[dict(zip(("mount", "spacetotal", "spaceavail"), l.split())) for l in fileinput.input()]}, indent=2)'  | sed '1d; $d; s/^ *//'` 2> /dev/null

printf '{"ServerName":"%s","distro":"%s","uptime":"%s","IPAddress":"%s","PublicIP":"%s","NumberOfCPUs":"%s","CPULoad":"%s","TotalMemoryMB":"%s","MemoryInUse":"%s","FreeMemory":"%s", %s}\n' "$hostname" "$distro" "$uptime" "$IPAddress" "$PublicIP" "$NumberOfCPUs" "$CPULoad" "$TotalMemoryMB" "$MemoryInUse" "$FreeMemory" "$Disks"

